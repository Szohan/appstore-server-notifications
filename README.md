# AppStore Server Notification Convertor
Request handle and parse AppStore Server Notification to object.

Detailed information about the request can be found on the official website:
https://developer.apple.com/documentation/appstoreservernotifications
## Installation
This project using composer.
```
$ composer require szohan/appstore-server-notifications
```

## Usage
Get payloadSigned how object.

```php
<?php

use Szohan\AppstoreServerNotifications\Api\AppStoreNotification;
________________

$appStoreNotification = new AppStoreNotification($rawSignedPayload);
$signedPayload = $appStoreNotification->getPayload();

var_dump($signedPayload);
```