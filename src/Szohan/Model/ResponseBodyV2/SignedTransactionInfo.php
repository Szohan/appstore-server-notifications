<?php
namespace Szohan\AppstoreServerNotifications\Model\ResponseBodyV2;

class SignedTransactionInfo
{
    private $appAccountToken;
    private $bundleId;
    private $expiresDate;
    private $inAppOwnershipType;
    private $isUpgraded;
    private $offerIdentifier;
    private $offerType;
    private $originalPurchaseDate;
    private $originalTransactionId;
    private $productId;
    private $purchaseDate;
    private $quantity;
    private $revocationDate;
    private $revocationReason;
    private $signedDate;
    private $subscriptionGroupIdentifier;
    private $transactionId;
    private $type;
    private $webOrderLineItemId;

    /**
     * @return mixed
     */
    public function getAppAccountToken()
    {
        return $this->appAccountToken;
    }

    /**
     * @param mixed $appAccountToken
     */
    public function setAppAccountToken($appAccountToken): void
    {
        $this->appAccountToken = $appAccountToken;
    }

    /**
     * @return mixed
     */
    public function getBundleId()
    {
        return $this->bundleId;
    }

    /**
     * @param mixed $bundleId
     */
    public function setBundleId($bundleId): void
    {
        $this->bundleId = $bundleId;
    }

    /**
     * @return mixed
     */
    public function getExpiresDate()
    {
        return $this->expiresDate;
    }

    /**
     * @param mixed $expiresDate
     */
    public function setExpiresDate($expiresDate): void
    {
        $this->expiresDate = $expiresDate;
    }

    /**
     * @return mixed
     */
    public function getInAppOwnershipType()
    {
        return $this->inAppOwnershipType;
    }

    /**
     * @param mixed $inAppOwnershipType
     */
    public function setInAppOwnershipType($inAppOwnershipType): void
    {
        $this->inAppOwnershipType = $inAppOwnershipType;
    }

    /**
     * @return mixed
     */
    public function getIsUpgraded()
    {
        return $this->isUpgraded;
    }

    /**
     * @param mixed $isUpgraded
     */
    public function setIsUpgraded($isUpgraded): void
    {
        $this->isUpgraded = $isUpgraded;
    }

    /**
     * @return mixed
     */
    public function getOfferIdentifier()
    {
        return $this->offerIdentifier;
    }

    /**
     * @param mixed $offerIdentifier
     */
    public function setOfferIdentifier($offerIdentifier): void
    {
        $this->offerIdentifier = $offerIdentifier;
    }

    /**
     * @return mixed
     */
    public function getOfferType()
    {
        return $this->offerType;
    }

    /**
     * @param mixed $offerType
     */
    public function setOfferType($offerType): void
    {
        $this->offerType = $offerType;
    }

    /**
     * @return mixed
     */
    public function getOriginalPurchaseDate()
    {
        return $this->originalPurchaseDate;
    }

    /**
     * @param mixed $originalPurchaseDate
     */
    public function setOriginalPurchaseDate($originalPurchaseDate): void
    {
        $this->originalPurchaseDate = $originalPurchaseDate;
    }

    /**
     * @return mixed
     */
    public function getOriginalTransactionId()
    {
        return $this->originalTransactionId;
    }

    /**
     * @param mixed $originalTransactionId
     */
    public function setOriginalTransactionId($originalTransactionId): void
    {
        $this->originalTransactionId = $originalTransactionId;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId): void
    {
        $this->productId = $productId;
    }

    /**
     * @return mixed
     */
    public function getPurchaseDate()
    {
        return $this->purchaseDate;
    }

    /**
     * @param mixed $purchaseDate
     */
    public function setPurchaseDate($purchaseDate): void
    {
        $this->purchaseDate = $purchaseDate;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getRevocationDate()
    {
        return $this->revocationDate;
    }

    /**
     * @param mixed $revocationDate
     */
    public function setRevocationDate($revocationDate): void
    {
        $this->revocationDate = $revocationDate;
    }

    /**
     * @return mixed
     */
    public function getRevocationReason()
    {
        return $this->revocationReason;
    }

    /**
     * @param mixed $revocationReason
     */
    public function setRevocationReason($revocationReason): void
    {
        $this->revocationReason = $revocationReason;
    }

    /**
     * @return mixed
     */
    public function getSignedDate()
    {
        return $this->signedDate;
    }

    /**
     * @param mixed $signedDate
     */
    public function setSignedDate($signedDate): void
    {
        $this->signedDate = $signedDate;
    }

    /**
     * @return mixed
     */
    public function getSubscriptionGroupIdentifier()
    {
        return $this->subscriptionGroupIdentifier;
    }

    /**
     * @param mixed $subscriptionGroupIdentifier
     */
    public function setSubscriptionGroupIdentifier($subscriptionGroupIdentifier): void
    {
        $this->subscriptionGroupIdentifier = $subscriptionGroupIdentifier;
    }

    /**
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * @param mixed $transactionId
     */
    public function setTransactionId($transactionId): void
    {
        $this->transactionId = $transactionId;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getWebOrderLineItemId()
    {
        return $this->webOrderLineItemId;
    }

    /**
     * @param mixed $webOrderLineItemId
     */
    public function setWebOrderLineItemId($webOrderLineItemId): void
    {
        $this->webOrderLineItemId = $webOrderLineItemId;
    }

}