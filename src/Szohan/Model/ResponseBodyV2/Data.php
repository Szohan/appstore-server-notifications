<?php

namespace Szohan\AppstoreServerNotifications\Model\ResponseBodyV2;

class Data
{
    private $appAppleId;
    private $bundleId;
    private $bundleVersion;
    private $environment;
    private SignedRenewalInfo $signedRenewalInfo;
    private SignedTransactionInfo $signedTransactionInfo;

    /**
     * @return mixed
     */
    public function getAppAppleId()
    {
        return $this->appAppleId;
    }

    /**
     * @param mixed $appAppleId
     */
    public function setAppAppleId($appAppleId): void
    {
        $this->appAppleId = $appAppleId;
    }

    /**
     * @return mixed
     */
    public function getBundleId()
    {
        return $this->bundleId;
    }

    /**
     * @param mixed $bundleId
     */
    public function setBundleId($bundleId): void
    {
        $this->bundleId = $bundleId;
    }

    /**
     * @return mixed
     */
    public function getBundleVersion()
    {
        return $this->bundleVersion;
    }

    /**
     * @param mixed $bundleVersion
     */
    public function setBundleVersion($bundleVersion): void
    {
        $this->bundleVersion = $bundleVersion;
    }

    /**
     * @return mixed
     */
    public function getEnvironment()
    {
        return $this->environment;
    }

    /**
     * @param mixed $environment
     */
    public function setEnvironment($environment): void
    {
        $this->environment = $environment;
    }

    /**
     * @return SignedRenewalInfo
     */
    public function getSignedRenewalInfo(): SignedRenewalInfo
    {
        return $this->signedRenewalInfo;
    }

    /**
     * @param SignedRenewalInfo $signedRenewalInfo
     */
    public function setSignedRenewalInfo(SignedRenewalInfo $signedRenewalInfo): void
    {
        $this->signedRenewalInfo = $signedRenewalInfo;
    }

    /**
     * @return SignedTransactionInfo
     */
    public function getSignedTransactionInfo(): SignedTransactionInfo
    {
        return $this->signedTransactionInfo;
    }

    /**
     * @param SignedTransactionInfo $signedTransactionInfo
     */
    public function setSignedTransactionInfo(SignedTransactionInfo $signedTransactionInfo): void
    {
        $this->signedTransactionInfo = $signedTransactionInfo;
    }
}