<?php

namespace Szohan\AppstoreServerNotifications\Model\ResponseBodyV2;

class SignedPayload
{
    private $notificationType;
    private $signedDate;
    private $subtype;
    private $notificationUUID;
    private $notificationVersion;
    private $version;
    private Data $data;


    /**
     * @return mixed
     */
    public function getNotificationType()
    {
        return $this->notificationType;
    }

    /**
     * @param mixed $notificationType
     */
    public function setNotificationType($notificationType): void
    {
        $this->notificationType = $notificationType;
    }

    /**
     * @return mixed
     */
    public function getSignedDate()
    {
        return $this->signedDate;
    }

    /**
     * @param mixed $signedDate
     */
    public function setSignedDate($signedDate): void
    {
        $this->signedDate = $signedDate;
    }

    /**
     * @return mixed
     */
    public function getSubtype()
    {
        return $this->subtype;
    }

    /**
     * @param mixed $subtype
     */
    public function setSubtype($subtype): void
    {
        $this->subtype = $subtype;
    }

    /**
     * @return mixed
     */
    public function getNotificationUUID()
    {
        return $this->notificationUUID;
    }

    /**
     * @param mixed $notificationUUID
     */
    public function setNotificationUUID($notificationUUID): void
    {
        $this->notificationUUID = $notificationUUID;
    }

    /**
     * @return mixed
     */
    public function getNotificationVersion()
    {
        return $this->notificationVersion;
    }

    /**
     * @param mixed $notificationVersion
     */
    public function setNotificationVersion($notificationVersion): void
    {
        $this->notificationVersion = $notificationVersion;
    }

    /**
     * @return Data
     */
    public function getData(): Data
    {
        return $this->data;
    }

    /**
     * @param Data $data
     */
    public function setData(Data $data): void
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     */
    public function setVersion($version): void
    {
        $this->version = $version;
    }


}