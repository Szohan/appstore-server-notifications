<?php

namespace Szohan\AppstoreServerNotifications\Model\ResponseBodyV2;

class SignedRenewalInfo
{
    private $autoRenewProductId;
    private $autoRenewStatus;
    private $expirationIntent;
    private $gracePeriodExpiresDate;
    private $isInBillingRetryPeriod;
    private $offerIdentifier;
    private $offerType;
    private $originalTransactionId;
    private $priceIncreaseStatus;
    private $productId;
    private $signedDate;

    /**
     * @return mixed
     */
    public function getAutoRenewProductId()
    {
        return $this->autoRenewProductId;
    }

    /**
     * @param mixed $autoRenewProductId
     */
    public function setAutoRenewProductId($autoRenewProductId): void
    {
        $this->autoRenewProductId = $autoRenewProductId;
    }

    /**
     * @return mixed
     */
    public function getAutoRenewStatus()
    {
        return $this->autoRenewStatus;
    }

    /**
     * @param mixed $autoRenewStatus
     */
    public function setAutoRenewStatus($autoRenewStatus): void
    {
        $this->autoRenewStatus = $autoRenewStatus;
    }

    /**
     * @return mixed
     */
    public function getExpirationIntent()
    {
        return $this->expirationIntent;
    }

    /**
     * @param mixed $expirationIntent
     */
    public function setExpirationIntent($expirationIntent): void
    {
        $this->expirationIntent = $expirationIntent;
    }

    /**
     * @return mixed
     */
    public function getGracePeriodExpiresDate()
    {
        return $this->gracePeriodExpiresDate;
    }

    /**
     * @param mixed $gracePeriodExpiresDate
     */
    public function setGracePeriodExpiresDate($gracePeriodExpiresDate): void
    {
        $this->gracePeriodExpiresDate = $gracePeriodExpiresDate;
    }

    /**
     * @return mixed
     */
    public function getIsInBillingRetryPeriod()
    {
        return $this->isInBillingRetryPeriod;
    }

    /**
     * @param mixed $isInBillingRetryPeriod
     */
    public function setIsInBillingRetryPeriod($isInBillingRetryPeriod): void
    {
        $this->isInBillingRetryPeriod = $isInBillingRetryPeriod;
    }

    /**
     * @return mixed
     */
    public function getOfferIdentifier()
    {
        return $this->offerIdentifier;
    }

    /**
     * @param mixed $offerIdentifier
     */
    public function setOfferIdentifier($offerIdentifier): void
    {
        $this->offerIdentifier = $offerIdentifier;
    }

    /**
     * @return mixed
     */
    public function getOfferType()
    {
        return $this->offerType;
    }

    /**
     * @param mixed $offerType
     */
    public function setOfferType($offerType): void
    {
        $this->offerType = $offerType;
    }

    /**
     * @return mixed
     */
    public function getOriginalTransactionId()
    {
        return $this->originalTransactionId;
    }

    /**
     * @param mixed $originalTransactionId
     */
    public function setOriginalTransactionId($originalTransactionId): void
    {
        $this->originalTransactionId = $originalTransactionId;
    }

    /**
     * @return mixed
     */
    public function getPriceIncreaseStatus()
    {
        return $this->priceIncreaseStatus;
    }

    /**
     * @param mixed $priceIncreaseStatus
     */
    public function setPriceIncreaseStatus($priceIncreaseStatus): void
    {
        $this->priceIncreaseStatus = $priceIncreaseStatus;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId): void
    {
        $this->productId = $productId;
    }

    /**
     * @return mixed
     */
    public function getSignedDate()
    {
        return $this->signedDate;
    }

    /**
     * @param mixed $signedDate
     */
    public function setSignedDate($signedDate): void
    {
        $this->signedDate = $signedDate;
    }
}