<?php

namespace Szohan\AppstoreServerNotifications\Service;

class RawSignedPayload
{
    private string $rawSignedPayload;

    public function __construct(string $rawSignedPayload)
    {
        $this->rawSignedPayload = $rawSignedPayload;
    }

    public function getRawHeaderJws():?string
    {
        return explode('.', $this->rawSignedPayload)[0] ?? null;
    }

    public function getRawPayloadJws():?string
    {
        return explode('.', $this->rawSignedPayload)[1] ?? null;
    }

    public function getSignatureJws():?string
    {
        return explode('.', $this->rawSignedPayload)[3] ?? null;
    }

    public function getDecodeRawHeaderJws()
    {
        return $this->decode($this->getRawHeaderJws());
    }

    public function getDecodeRawPayloadJws()
    {
        return $this->decode($this->getRawPayloadJws());
    }

    public function decode($entry)
    {
        return base64_decode($entry);
    }

}