<?php

namespace Szohan\AppstoreServerNotifications\Service;

use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Szohan\AppstoreServerNotifications\Api\AppStoreNotificationException;

class VerifySignature
{
    private  string $urlRootCert;
    private string $rawSignedPayload;

    public function __construct(string $rawSignedPayload, string $urlRootCert)
    {
        $this->rawSignedPayload = $rawSignedPayload;
        $this->urlRootCert = $urlRootCert;
    }


    /**
     * @throws AppStoreNotificationException
     */
    public function isVerify():bool
    {
        $publicKey = $this->der2pem(file_get_contents($this->urlRootCert));

        $header = json_decode((new RawSignedPayload($this->rawSignedPayload))->getDecodeRawHeaderJws(), true);

        if (!$certsX5C = $header['x5c']) {
            throw new AppStoreNotificationException("Not found header certificate!");
        }

        if (!$certVerifSignature = $certsX5C[0]) {
            throw new AppStoreNotificationException("Not found certificate for verify signature!");
        }

        if (!$this->isVerifySignature($this->rawSignedPayload,  $this->str2pem($certVerifSignature))){
            return false;
        }

        foreach ($certsX5C as $str) {
            if (TRUE === $this->isVerifyCertificate($this->str2pem($str), $publicKey)){
                return true;
            }
        }

        return false;
    }

    private function isVerifySignature($jws, $certificate): bool
    {
        try {
            JWT::decode($jws, new Key($certificate, 'ES256'));
            return true;
        } catch (Exception $e){
            return false;
        }

    }

    private function isVerifyCertificate($certificate, $publicKey): bool
    {

        $result = openssl_x509_verify($certificate, $publicKey);

        if ($result === 1) {
            return true;
        } elseif ($result === 0){
            return false;
        } elseif ($result === -1){
            throw new AppStoreNotificationException("Error verified certificate!");
        } else {
            return false;
        }

    }

    private function str2pem($str){
        return '-----BEGIN CERTIFICATE-----' . PHP_EOL
            . chunk_split($str, 64, PHP_EOL)
            . '-----END CERTIFICATE-----' . PHP_EOL
        ;
    }


    private function der2pem($der_data) {
        $pem = chunk_split(base64_encode($der_data), 64, "\n");
        $pem = "-----BEGIN CERTIFICATE-----\n".$pem."-----END CERTIFICATE-----\n";
        return $pem;
    }
}