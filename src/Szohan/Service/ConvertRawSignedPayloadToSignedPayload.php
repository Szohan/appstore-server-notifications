<?php

namespace Szohan\AppstoreServerNotifications\Service;

use Szohan\AppstoreServerNotifications\Model\ResponseBodyV2\Data;
use Szohan\AppstoreServerNotifications\Model\ResponseBodyV2\SignedPayload;
use Szohan\AppstoreServerNotifications\Model\ResponseBodyV2\SignedRenewalInfo;
use Szohan\AppstoreServerNotifications\Model\ResponseBodyV2\SignedTransactionInfo;

class ConvertRawSignedPayloadToSignedPayload
{
    private int $version;
    private string $rawSignedPayload;

    public function __construct(string $rawSignedPayload, int $version = 2)
    {
        $this->rawSignedPayload = $rawSignedPayload;
        $this->version = $version;
    }

    public function convertation(): ?SignedPayload
    {
        return $this->createSignedPayload($this->rawSignedPayload);
    }

    private function createSignedPayload($rawSignedPayload): SignedPayload
    {

        $jsonSignedPayload = json_decode((new RawSignedPayload($rawSignedPayload))->getDecodeRawPayloadJws());

        $signedPayload = new SignedPayload();

        $signedPayload->setSubtype($jsonSignedPayload->subtype ?? null);
        $signedPayload->setNotificationType($jsonSignedPayload->notificationType ?? null);
        $signedPayload->setNotificationVersion($jsonSignedPayload->notificationVersion ?? null);
        $signedPayload->setNotificationUUID($jsonSignedPayload->notificationUUID ?? null);
        $signedPayload->setSignedDate($jsonSignedPayload->signedDate ?? null);

        $signedPayload->setData($this->createData($jsonSignedPayload->data ?? null));

        $signedPayload->setVersion($jsonSignedPayload->version ?? null);

        return $signedPayload;
    }

    private function createData($rawData):Data
    {
        $data = new Data();
        $data->setAppAppleId($rawData->appAppleId ?? null);
        $data->setBundleId($rawData->bundleId ?? null);
        $data->setBundleVersion($rawData->bundleVersion ?? null);
        $data->setEnvironment($rawData->environment ?? null);
        $data->setSignedRenewalInfo($this->createSignedRenewalInfo($rawData->signedRenewalInfo ?? null));
        $data->setSignedTransactionInfo($this->createSignedTransactionInfo($rawData->signedTransactionInfo ?? null));

        return $data;
    }


    private function createSignedRenewalInfo(string $rawSignedRenewalInfo):SignedRenewalInfo
    {
        $jsonSignedRenewalInfo = json_decode((new RawSignedPayload($rawSignedRenewalInfo))->getDecodeRawPayloadJws());

        $signedRenewalInfo = new SignedRenewalInfo();
        $signedRenewalInfo->setAutoRenewProductId($jsonSignedRenewalInfo->autoRenewProductId ?? null);
        $signedRenewalInfo->setAutoRenewStatus($jsonSignedRenewalInfo->autoRenewStatus ?? null);
        $signedRenewalInfo->setExpirationIntent($jsonSignedRenewalInfo->expirationIntent ?? null);
        $signedRenewalInfo->setGracePeriodExpiresDate($jsonSignedRenewalInfo->gracePeriodExpiresDate ?? null);
        $signedRenewalInfo->setIsInBillingRetryPeriod($jsonSignedRenewalInfo->isInBillingRetryPeriod ?? null);
        $signedRenewalInfo->setOfferIdentifier($jsonSignedRenewalInfo->offerIdentifier ?? null);
        $signedRenewalInfo->setOfferType($jsonSignedRenewalInfo->offerType ?? null);
        $signedRenewalInfo->setOriginalTransactionId($jsonSignedRenewalInfo->originalTransactionId ?? null);
        $signedRenewalInfo->setPriceIncreaseStatus($jsonSignedRenewalInfo->priceIncreaseStatus ?? null);
        $signedRenewalInfo->setProductId($jsonSignedRenewalInfo->productId ?? null);
        $signedRenewalInfo->setSignedDate($jsonSignedRenewalInfo->signedDate ?? null);

        return $signedRenewalInfo;
    }

    private function createSignedTransactionInfo($rawSignedTransactionInfo):SignedTransactionInfo
    {
        $jsonSignedTransactionInfo = json_decode((new RawSignedPayload($rawSignedTransactionInfo))->getDecodeRawPayloadJws());

        $signedTransactionInfo = new SignedTransactionInfo();

        $signedTransactionInfo->setSignedDate($jsonSignedTransactionInfo->signedDate ?? null);
        $signedTransactionInfo->setProductId($jsonSignedTransactionInfo->productId ?? null);
        $signedTransactionInfo->setOriginalTransactionId($jsonSignedTransactionInfo->originalTransactionId ?? null);
        $signedTransactionInfo->setOfferIdentifier($jsonSignedTransactionInfo->offerIdentifier ?? null);
        $signedTransactionInfo->setBundleId($jsonSignedTransactionInfo->bundleId ?? null);
        $signedTransactionInfo->setAppAccountToken($jsonSignedTransactionInfo->appAccountToken ?? null);
        $signedTransactionInfo->setType($jsonSignedTransactionInfo->type ?? null);
        $signedTransactionInfo->setInAppOwnershipType($jsonSignedTransactionInfo->inAppOwnershipType ?? null);
        $signedTransactionInfo->setIsUpgraded($jsonSignedTransactionInfo->isUpgraded ?? null);
        $signedTransactionInfo->setOfferType($jsonSignedTransactionInfo->offerType ?? null);
        $signedTransactionInfo->setOriginalPurchaseDate($jsonSignedTransactionInfo->originalPurchaseDate ?? null);
        $signedTransactionInfo->setPurchaseDate($jsonSignedTransactionInfo->purchaseDate ?? null);
        $signedTransactionInfo->setQuantity($jsonSignedTransactionInfo->quantity ?? null);
        $signedTransactionInfo->setRevocationDate($jsonSignedTransactionInfo->revocationDate ?? null);
        $signedTransactionInfo->setSubscriptionGroupIdentifier($jsonSignedTransactionInfo->subscriptionGroupIdentifier ?? null);
        $signedTransactionInfo->setExpiresDate($jsonSignedTransactionInfo->expiresDate ?? null);
        $signedTransactionInfo->setRevocationReason($jsonSignedTransactionInfo->revocationReason ?? null);
        $signedTransactionInfo->setWebOrderLineItemId($jsonSignedTransactionInfo->webOrderLineItemId ?? null);
        $signedTransactionInfo->setTransactionId($jsonSignedTransactionInfo->transactionId ?? null);

        return $signedTransactionInfo;
    }

}