<?php
namespace Szohan\AppstoreServerNotifications\Api;

use Szohan\AppstoreServerNotifications\Model\ResponseBodyV2\SignedPayload;
use Szohan\AppstoreServerNotifications\Service\ConvertRawSignedPayloadToSignedPayload;
use Szohan\AppstoreServerNotifications\Service\RawSignedPayload;
use Szohan\AppstoreServerNotifications\Service\VerifySignature;

class AppStoreNotification
{
    const DEFAULT_REPONSE_BODY = 2;
    const URL_CA_ROOT_CERTIFICATE = "https://www.apple.com/certificateauthority/AppleRootCA-G3.cer";

    private string $rawSignedPayload;
    private int $version;
    private string $urlRootCert;

    public function __construct(
        string $rawSignedPayload,
        $version = self::DEFAULT_REPONSE_BODY,
        $urlRootCert = self::URL_CA_ROOT_CERTIFICATE
    )
    {
        $this->rawSignedPayload = $rawSignedPayload;
        $this->version = $version;
        $this->urlRootCert = $urlRootCert;
    }

    /**
     * @throws AppStoreNotificationException
     */
    public function getPayload():SignedPayload
    {
        $verifySignature = new VerifySignature($this->rawSignedPayload, $this->urlRootCert);

        if (!$verifySignature->isVerify()){
            throw new AppStoreNotificationException("Signature not verified");
        }

        $convertor = new ConvertRawSignedPayloadToSignedPayload($this->rawSignedPayload, $this->version);
        return $convertor->convertation();
    }
}